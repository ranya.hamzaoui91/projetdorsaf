import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TesterrrComponent } from './testerrr.component';

describe('TesterrrComponent', () => {
  let component: TesterrrComponent;
  let fixture: ComponentFixture<TesterrrComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TesterrrComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TesterrrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
