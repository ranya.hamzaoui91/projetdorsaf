import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiarticleService {

  constructor(private http:HttpClient) { }


  getarticles() {
    return this.http.get(`${environment.baseUrl}/users/article/All`)
  }

  getarticlebyID(id: any) {
    return this.http.get(`${environment.baseUrl}/users/article/get/${id}`)
  }

  getarticlebyPage(page: any,size:any) {
    return this.http.get(`${environment.baseUrl}/users/article/getByPage`, {params:{size:size, page:page}})
  }

  // deletearticle(id: any) {
  //   return this.http.delete(`${environment.baseUrl}/users/article/delete/${id}`, { headers: this.httpheaders })
  // }
}
