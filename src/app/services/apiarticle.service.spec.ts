import { TestBed } from '@angular/core/testing';

import { ApiarticleService } from './apiarticle.service';

describe('ApiarticleService', () => {
  let service: ApiarticleService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ApiarticleService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
