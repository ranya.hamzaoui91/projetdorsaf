import { TestBed } from '@angular/core/testing';

import { ApimarqueService } from './apimarque.service';

describe('ApimarqueService', () => {
  let service: ApimarqueService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ApimarqueService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
