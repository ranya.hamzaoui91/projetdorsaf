import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Cart } from 'src/models/cart';
import Swal from 'sweetalert2';
import { CartComponent } from '../components/cart/cart.component';

@Injectable({
  providedIn: 'root'
})
export class CartService {
  client=JSON.parse(localStorage.getItem("currentuser"))
  public cartListSubject = new BehaviorSubject<any>([]);
  public toggleCartSubject = new BehaviorSubject(false);
  constructor(private http:HttpClient){}

  toggleCart = () => {
      this.toggleCartSubject.next(!this.toggleCartSubject.getValue());
  };

  addToCart = (cart:Cart) => {
      let current = this.cartListSubject.getValue();
      let dup = current.find((c:any)=>c.article.id === cart.article.id);
      if(dup) dup.quantity += cart.quantity;
      
      else current.push(cart);
      this.addtocartback(cart).subscribe(res=>{
        console.log('carttttt',cart)
        console.log('reslttt',res)
        Swal.fire({
          title: 'addtocart'})


      })
      this.cartListSubject.next(current);
  };
  
  reloadCart = (cartList:any) => {
      this.cartListSubject.next(cartList);
  };

  removeCart = (index:any,cart:Cart) => {
      let current = this.cartListSubject.getValue();
      current.splice(index,1);
      this.removetocartback(cart).subscribe(res=>{
        console.log('reslt',res)
        Swal.fire({
          title: 'removetocart'})


      })
      this.cartListSubject.next(current);
  };

  addtocartback(cart:Cart){

    return this.http.post(`${environment.baseUrl}/users/cart/addtocart/${this.client.username}`,
     {
       username:this.client.username,
       itemId:cart.article.id,
       quantity:cart.quantity
     }
    )

  }

  removetocartback(cart:Cart){
    return this.http.post(`${environment.baseUrl}/users/cart/removeFromCart`,{
      username:this.client.username,
      itemId:cart.article.id,
      quantity:cart.quantity
    })

  }

  
}
