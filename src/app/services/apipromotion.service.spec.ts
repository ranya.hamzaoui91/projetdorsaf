import { TestBed } from '@angular/core/testing';

import { ApipromotionService } from './apipromotion.service';

describe('ApipromotionService', () => {
  let service: ApipromotionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ApipromotionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
