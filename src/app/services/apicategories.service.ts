import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApicategoriesService {
  httpheaders: HttpHeaders | { [header: string]: string | string[]; };
  constructor(private http:HttpClient) { }

  getcategories(){
    return this.http.get(`${environment.baseUrl}/users/categories/All`)
  }

  getcategoriesID(id:any){
    return this.http.get(`${environment.baseUrl}/users/categories/get/${id}`, { headers: this.httpheaders })
  }
  addcategories(categories:any){
    return this.http.post(`${environment.baseUrl}/users/categories/Save`,categories)
  }

  deletecategories(id:any){
    return this.http.delete(`${environment.baseUrl}/users/categories/delete/${id}`)
  }
  updatecategories (id:any, newacategories:any){
    return this.http.put(`${environment.baseUrl}/users/categories/put/${id}`, newacategories)

  }
 

}
