import { TestBed } from '@angular/core/testing';

import { ApiauthenticateService } from './apiauthenticate.service';

describe('ApiauthenticateService', () => {
  let service: ApiauthenticateService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ApiauthenticateService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
