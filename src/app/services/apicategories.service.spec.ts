import { TestBed } from '@angular/core/testing';

import { ApicategoriesService } from './apicategories.service';

describe('ApicategoriesService', () => {
  let service: ApicategoriesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ApicategoriesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
