import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiauthenticateService {

  constructor(private http:HttpClient) {}

  login(user:any){
    return this.http.post(`${environment.baseUrl}/users/login`,user)
  }
  register(client:any){ 
  return this.http.post(`${environment.baseUrl}/users/saveclient/2`,client)
 }
 forgetpassword(email:any){
  return this.http.get(`${environment.baseUrl}/users/client/forgetpassword`, {params:{'email':email}})
 }

 resetpassword(newPassword:any, resetToken:String){

  return this.http.get(`${environment.baseUrl}/users/client/savePassword/${resetToken}`,
  {params:{'newPassword':newPassword}})


 }
}
