import { TestBed } from '@angular/core/testing';

import { ApicommandeService } from './apicommande.service';

describe('ApicommandeService', () => {
  let service: ApicommandeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ApicommandeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
