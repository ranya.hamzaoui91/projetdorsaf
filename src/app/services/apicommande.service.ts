import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApicommandeService {

  constructor(private http:HttpClient) { }

  getcommande(){
    return this.http.get(`${environment.baseUrl}/users/commande/All`)
  }

  addcommande(commande:any,username:any){
    return this.http.post(`http://localhost:8081/users/commande/submit/${username}`,commande)
  }

  deletecommande(id: any) {
     return this.http.delete(`${environment.baseUrl}/users/commande/delete/${id}`)
     }
}
