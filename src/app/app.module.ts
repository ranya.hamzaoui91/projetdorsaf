import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { LayoutComponent } from './components/layout/layout.component';
import { FooterComponent } from './components/footer/footer.component';
import { HomeComponent } from './components/home/home.component';
import { ArticleComponent } from './components/article/article.component';
import { DetailsarticleComponent } from './components/detailsarticle/detailsarticle.component';
import {HttpClientModule} from '@angular/common/http';
import { CategoriesComponent } from './components/categories/categories.component';
import { QuantitecontrolComponent } from './components/quantitecontrol/quantitecontrol.component';
import { CartComponent } from './components/cart/cart.component';
import { CheckoutComponent } from './components/checkout/checkout.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProfilComponent } from './components/profil/profil.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { NgxPaginationModule } from 'ngx-pagination';

import { ResetpasswordComponent } from './components/resetpassword/resetpassword.component';
import { ForgetpasswordComponent } from './components/forgetpassword/forgetpassword.component';
import { Ng5SliderModule } from 'ng5-slider';
import { RecherchePipe } from './recherche.pipe';
import { ContactComponent } from './components/contact/contact.component';
import { ListfavorisComponent } from './components/listfavoris/listfavoris.component';
import { TesterrrComponent } from './testerrr/testerrr.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    LayoutComponent,
    FooterComponent,
    HomeComponent,
    ArticleComponent,
    DetailsarticleComponent,
    CategoriesComponent,
    QuantitecontrolComponent,
    CartComponent,
    CheckoutComponent,
    ProfilComponent,
    LoginComponent,
    RegisterComponent,
    ResetpasswordComponent,
    ForgetpasswordComponent,
    RecherchePipe,
    ContactComponent,
    ListfavorisComponent,
    TesterrrComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    Ng5SliderModule







    
  ],
  providers: [],
  schemas:[CUSTOM_ELEMENTS_SCHEMA],
  bootstrap: [AppComponent]
})
export class AppModule { }
