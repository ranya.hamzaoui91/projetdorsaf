import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ArticleComponent } from './components/article/article.component';
import { CartComponent } from './components/cart/cart.component';
import { CheckoutComponent } from './components/checkout/checkout.component';
import { ContactComponent } from './components/contact/contact.component';
import { DetailsarticleComponent } from './components/detailsarticle/detailsarticle.component';
import { ForgetpasswordComponent } from './components/forgetpassword/forgetpassword.component';
import { HomeComponent } from './components/home/home.component';
import { LayoutComponent } from './components/layout/layout.component';
import { ListfavorisComponent } from './components/listfavoris/listfavoris.component';
import { LoginComponent } from './components/login/login.component';
import { ProfilComponent } from './components/profil/profil.component';
import { QuantitecontrolComponent } from './components/quantitecontrol/quantitecontrol.component';
import { RegisterComponent } from './components/register/register.component';
import { ResetpasswordComponent } from './components/resetpassword/resetpassword.component';
import { TesterrrComponent } from './testerrr/testerrr.component';

const routes: Routes = [
{path:"",component:HomeComponent, children:[
{path:"",component:LayoutComponent},
{path:"article",component:ArticleComponent},
{path:"detailarticle/:id", component:DetailsarticleComponent},
{path:"quantitecontrol", component:QuantitecontrolComponent},
{path:"cart",component:CartComponent},
{path:"profil", component:ProfilComponent},
{path:"login",component:LoginComponent},
{path:"checkout" , component:CheckoutComponent},
{path:"register" , component:RegisterComponent},

{path:'resetpassword/:resetToken',component:ResetpasswordComponent},
{path:"forgetpasswd",component:ForgetpasswordComponent},
{path:"contact", component:ContactComponent},
{path:"listfavoris",component:ListfavorisComponent},
{path:"tester",component:TesterrrComponent},





  ]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
