import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiauthenticateService } from 'src/app/services/apiauthenticate.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  submitted:boolean=false;

  fileToUpload: Array<File> = [];

  constructor(private formBuilder: FormBuilder, private apiauth: ApiauthenticateService, private route: Router) { }

  ngOnInit(): void {
    this.registerForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      codeclient: ['', Validators.required],
      username: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      phone: ['', [Validators.required, Validators.minLength(8)]],
      file: ['', Validators.required]

    });
  }

  get f() { return this.registerForm.controls; }
  


  onSubmit() {
    this.submitted = true ;


        // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    }
    console.log(this.registerForm.value)

    var formdata = new FormData();
    
    formdata.append('firstname', this.registerForm.value.firstName);
    formdata.append('lastname', this.registerForm.value.lastName);
    formdata.append('codeclient', this.registerForm.value.codeclient);
    formdata.append('username', this.registerForm.value.username);
    formdata.append('email', this.registerForm.value.email);
    formdata.append('password', this.registerForm.value.password);
    formdata.append('phone', this.registerForm.value.phone);
    formdata.append('file', this.fileToUpload[0]);

    this.apiauth.register(formdata).subscribe(res => {
      
      console.log(res)
      Swal.fire({
        title: 'your client is successfully registered'
      })
      this.route.navigateByUrl('/login');
    },
      (err) => {
        Swal.fire({
          title: 'failed'
        })
      })

  }
  
  recuperphoto(file: any) {
    this.fileToUpload = <Array<File>>file.target.files;
    console.log(this.fileToUpload)

  }
  onReset() {
    this.submitted = false;
    this.registerForm.reset();
  }

}

