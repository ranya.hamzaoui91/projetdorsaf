import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApicommandeService } from 'src/app/services/apicommande.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.css']
})
export class ProfilComponent implements OnInit {
  user = JSON.parse(localStorage.getItem('currentuser'))
    Listcommande:any
  constructor(private commandeservice:ApicommandeService, private router:Router) { }

  ngOnInit(): void {
    this.getcommande()
  }
  
  getcommande(){
    console.log(this.user.username)
    this.commandeservice.getcommande().subscribe(res => {
      this.Listcommande = res
      this.Listcommande=this.Listcommande.filter((el:any)=>
      el.client.username==this.user.username)

      console.log('list order',res)
    })
}
getOccurence(index:any){
  return this.Listcommande.map(el=>el.articleList.map((el:any)=>el.nom))[index].reduce((prev:any, curr:any)=> (prev[curr] = ++ prev[curr] || 1, prev), {})
}

Removecommande(id: any) {


  Swal.fire({
    title: 'Are you sure?',
    text: "You won't be able to revert this!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes, delete it!'
  }).then((result) => {
    if (result.isConfirmed) {
      this.commandeservice.deletecommande(id).subscribe(res => {
        console.log(res)
        Swal.fire(
          'Deleted!',
          'Your file has been deleted.',
          'success'
        )
        this.getcommande()
      })


    }
  })

}


}
