import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QuantitecontrolComponent } from './quantitecontrol.component';

describe('QuantitecontrolComponent', () => {
  let component: QuantitecontrolComponent;
  let fixture: ComponentFixture<QuantitecontrolComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QuantitecontrolComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QuantitecontrolComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
