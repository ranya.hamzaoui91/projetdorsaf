import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CartService } from 'src/app/services/cart.service';

@Component({
  selector: 'app-quantitecontrol',
  templateUrl: './quantitecontrol.component.html',
  styleUrls: ['./quantitecontrol.component.css']
})
export class QuantitecontrolComponent implements OnInit {

  @Input() quantity: number;
  article:any
  @Output() onChange = new EventEmitter<number>();
  prix: any;
  cartList=[]
  constructor(private cartservice: CartService) { }

  ngOnInit():void { }
  plusOne = () =>{
      if (this.quantity < 1000){
          this.quantity++;
          this.onChange.emit(this.quantity);
      }
  };
  minusOne = () => {
      if (this.quantity > 1){
          this.quantity--;
          this.onChange.emit(this.quantity);
      }
  }
  addToCart = (article: any) => {
    if (this.quantity) {
      this.cartservice.addToCart({ article, quantity: this.quantity })
    }
  };

 
}

