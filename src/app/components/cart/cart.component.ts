import { Component, OnInit } from '@angular/core';
import { CartService } from 'src/app/services/cart.service';
import { Cart } from 'src/models/cart';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  public cartList: Cart[];

  public totalPrix: number=0;
  
  quantity=1;
  
  constructor(protected cartService: CartService) {
    this.loadCart();
    
  }


  ngOnInit(): void {
  }
  loadCart = () => {
    this.cartService.cartListSubject
      .subscribe(res => {
        this.cartList = res;
        let total = 0;
        for (let cart of this.cartList) {
          total += Number(cart.article.prix) * Number(cart.quantity);
        }
        this.totalPrix = total;
      })
      console.log('add to cart',this.cartList)
  };
  removeFromCart = (index:any,cart:Cart) => {
    this.cartService.removeCart(index,cart);
  };
  
  changequantity = (cart:any,quantity:any) => {
    cart.quantity = quantity;
    this.cartService.reloadCart(this.cartList);
}
  
   
   
}


