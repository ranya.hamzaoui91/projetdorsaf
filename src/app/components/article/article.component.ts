import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Options } from 'ng5-slider';
import { AttachSession } from 'protractor/built/driverProviders';
import { Subscription } from 'rxjs';
import { ApiarticleService } from 'src/app/services/apiarticle.service';
import { ApicategoriesService } from 'src/app/services/apicategories.service';
import { CartService } from 'src/app/services/cart.service';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css']
})
export class ArticleComponent implements OnInit {
  Listarticle: any
  submitted = false;
  filetoupload: any
  quantity: number = 1
  prix: any
  cartList = []
  size: number
  p: number = 1
  minValue = 50;
  maxValue = 2400;
  options: Options = {
    floor: 0,
    ceil: 2400
  };
  priceSelection: any;

  listcategories: any
  arraydataCategories : any;
  selectedcategories:any;
  FilterArticle:any
  selected_games2=[]


  articles: any = []
  filteredListarticle: any = [];
  f:any
  option: string;

  selectedChoice: any;
  constructor(private articleservice: ApiarticleService, private router: Router, private cartService: CartService, private categorieservice: ApicategoriesService) { }

  ngOnInit(): void {

    this.getarticle()
    this.getcategories()
  }

  getSelected2() {
    this.getarticle()
    console.log('listarticle',this.Listarticle); 
    
    this.selected_games2 = this.arraydataCategories.filter(s => {
      return s.isCkecked;
    });
    console.log('selected categories',this.selected_games2); 
    this.selected_games2=this.selected_games2.map(el=>el.id) 
    console.log('selected categories maaaap',this.selected_games2); 
    this.f=this.Listarticle.filter((el)=>this.selected_games2.includes(el.categories.id))
    this.Listarticle=this.f
    console.log('article with categories',this.Listarticle)
    console.log('final',this.Listarticle); 

  } 

  public loadExternalScript(url: string) {
    const body = <HTMLDivElement>document.body;
    const script = document.createElement('script');
    script.innerHTML = '';
    script.src = url;
    script.async = true;
    script.defer = true;
    body.appendChild(script);
  }

  handlefile(files: FileList) {
    this.filetoupload = files.item(0)
  }

  getarticlebyPage(size: any) {
    console.log('size', size.target.value)
    this.articleservice.getarticlebyPage(1, size.target.value).subscribe(res => {
      console.log('filter by page', res["content"])
      this.Listarticle = res["content"]
    })
  }

  getcategories() {

    this.categorieservice.getcategories().subscribe((res: any) => {

      this.arraydataCategories = res.map((el: any) => {
        var o = Object.assign({ isChecked: false }, el);
        return o;
      })
    


      console.log('categ', this.arraydataCategories)

    })
  }

getarticle() {

    this.articleservice.getarticles().subscribe(res => {
      this.Listarticle = res
      this.f=res
      // console.log('data', this.Listarticle)

    })
  }

getselectedcategories(){
console.log('categories',this.arraydataCategories)

this.selectedcategories=this.arraydataCategories.filter((el:any)=>{return el.isChecked})
console.log('change',this.arraydataCategories.filter((el:any)=>{return !el.isChecked}))

  }  

  changeqtestock = (quantity: number) => {
    this.quantity = quantity;
  };

  addToCart = (article: any) => {
    this.cartService.addToCart({ article, quantity: 1 })

    this.loadCart()
  };


  loadCart = () => {
    this.cartService.cartListSubject
      .subscribe(res => {
        this.cartList = res;
      })
    console.log('add to cart', this.cartList)
  };






  changePrice() {
    console.log('Price change', this.priceSelection);
    event = this.priceSelection
    if (event !== undefined) {
      const ListarticleByPrice = this.Listarticle.filter((elemt: any) => elemt.prix >= event[0] && elemt.prix <= event[1]);
      this.Listarticle = ListarticleByPrice;
    }
  }




  sortbyMessage(event: any): void {
    console.log('choice ...', event.target.value)
    this.option = event.target.value;
    // this.filteredarticles=this.articles
    const SortBy = (x: any, y: any) => {
      if (this.option === 'prix -- Low to High') {
        return ((x.prix === y.prix) ? 0 : ((x.prix > y.prix[this.option]) ? 1 : -1));
      } else if (this.option === 'prix -- High to Low') {
        return ((x.prix === y.prix) ? 0 : ((x.prix > y.prix) ? -1 : 1));
      } else if (this.option === 'Newest First') {
        return ((x.dateOfEntry === y.dateOfEntry) ? 0 : ((x.dateOfEntry > y.dateOfEntry) ? 1 : -1));
      } else {
        return ((x.dateOfEntry === y.dateOfEntry) ? 0 : ((x.dateOfEntry > y.dateOfEntry) ? 1 : -1));
      }
    };
    this.Listarticle = this.Listarticle.sort(SortBy);
    console.log(this.Listarticle)
  }


}




