import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ApiarticleService } from 'src/app/services/apiarticle.service';
import { ApicommandeService } from 'src/app/services/apicommande.service';
import { CartService } from 'src/app/services/cart.service';
import { Cart } from 'src/models/cart';
import Swal from 'sweetalert2';
import { ArticleComponent } from '../article/article.component';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {
  id=this.activteRoute.snapshot.params.id
  article:any
  public totalPrix: number=0;
  public cartList: Cart[];
  checkoutForm:FormGroup
  submitted:boolean=false
  client=JSON.parse(localStorage.getItem("currentuser"))
  
  constructor(private activteRoute:ActivatedRoute, 
    private commandeservice:ApicommandeService,
    private cartservice:CartService,
    private formBuilder:FormBuilder) { }

  ngOnInit(): void {
    this.loadCart()

    this.checkoutForm = this.formBuilder.group({
      date: ['', Validators.required],
      adresse: ['', Validators.required],
      city: ['', Validators.required],
      country: ['', Validators.required],
      phone: ['', Validators.required]

    });
    

  }



   
    loadCart = () => {
      this.cartservice.cartListSubject
        .subscribe(res => {
          this.cartList = res;
          let total = 0;
          for (let cart of this.cartList) {
            total += Number(cart.article.prix) * Number(cart.quantity);
          }
          this.totalPrix = total;
        })
        console.log('add to cart',this.cartList)
    };

    addOrder() {
       console.log("xx")
      this.submitted = true;

      // stop here if form is invalid
      if (this.checkoutForm.invalid) {
      console.log(this.checkoutForm.value)
          return;
      }


      this.commandeservice.addcommande(this.checkoutForm.value,this.client.username).subscribe(res => {
        console.log(res)

        Swal.fire({
          title:'addorder'

      })
    }

      )
  }}