import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApicategoriesService } from 'src/app/services/apicategories.service';
import { CartService } from 'src/app/services/cart.service';
import { Cart } from 'src/models/cart';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  Listcategorie:any
  public totalPrix: number=0;
  public cartList: Cart[];
  quantity=1;


  constructor(private categorieservice:ApicategoriesService, protected cartService:CartService, private route: Router) { 
    this.loadCart();
  }

  ngOnInit(): void {
    

this.getacategories()
  }
  getacategories() {

    this.categorieservice.getcategories().subscribe(res => {
      this.Listcategorie = res
      console.log('data',this.Listcategorie)

    })}


    loadCart = () => {
      this.cartService.cartListSubject
        .subscribe(res => {
          this.cartList = res;
          let total = 0;
          for (let cart of this.cartList) {
            total += Number(cart.article.prix) * Number(cart.quantity);
          }
          this.totalPrix = total;
        })
        console.log(this.cartList)
    };
   
    
    changequantity = (cart:any,quantity:any) => {
      cart.quantity = quantity;
      this.cartService.reloadCart(this.cartList);
  }
    
 
}
