import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiauthenticateService } from 'src/app/services/apiauthenticate.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-resetpassword',
  templateUrl: './resetpassword.component.html',
  styleUrls: ['./resetpassword.component.css']
})
export class ResetpasswordComponent implements OnInit {
  resetpasswordForm: FormGroup;
  resetToken = this.activatedroute.snapshot.params.resetToken;
  submitted: false;
  

  constructor(private apiauth: ApiauthenticateService,
    private activatedroute: ActivatedRoute,
    private router: Router, private fb: FormBuilder) { }

  ngOnInit(): void {
    this.resetpasswordForm = this.fb.group({
      password: ['', Validators.required],
      confirmpassword: ['', Validators.required]

    }, {
      validator: this.MustMatch('password', 'confirmpassword')
    });

  }
  get f() { return this.resetpasswordForm.controls; }

  resetpassword() {

    if (this.resetpasswordForm.invalid) {
      console.log(this.resetpasswordForm.value)
      return;
    }

    let password = this.resetpasswordForm.value.password
    console.log(password)
    this.apiauth.resetpassword(password, this.resetToken).subscribe(res => {
      console.log(res)
      this.router.navigateByUrl('/')
      Swal.fire({
        icon: 'success',
        text: 'Pasword has been changed '
      })

    })
  }

  MustMatch(controlName: string, matchingControlName: string) {
    return (formGroup: FormGroup) => {
      const control = formGroup.controls[controlName];
      const matchingControl = formGroup.controls[matchingControlName];

      if (matchingControl.errors && !matchingControl.errors.mustMatch) {
        // return if another validator has already found an error on the matchingControl
        return;
      }

      // set error on matchingControl if validation fails
      if (control.value !== matchingControl.value) {
        matchingControl.setErrors({ mustMatch: true });
      } else {
        matchingControl.setErrors(null);
      }
    }
  }
}
