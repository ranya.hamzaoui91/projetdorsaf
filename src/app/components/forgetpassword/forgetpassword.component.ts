import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiauthenticateService } from 'src/app/services/apiauthenticate.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-forgetpassword',
  templateUrl: './forgetpassword.component.html',
  styleUrls: ['./forgetpassword.component.css']
})
export class ForgetpasswordComponent implements OnInit {

  forgetpasswordForm:FormGroup ;
  submitted = false;
  forbiddenEmails: any;
  errorMessage: string;
  successMessage: string;
  IsvalidForm = true;

  constructor( private apiauth:ApiauthenticateService, private router:Router, private fb:FormBuilder ) { }

  ngOnInit(): void {
    this.forgetpasswordForm=this.fb.group({
      email: ['', Validators.required]
     
    });
  }
  
  get f() { return this.forgetpasswordForm.controls; }

  
  forgetpassword(){
    let email=this.forgetpasswordForm.value.email
    this.apiauth.forgetpassword(email).subscribe(res=>{
      console.log(res)
      Swal.fire({
        icon:'success',
        text:'Consulter vos boite Email'
      })

    })

  }

}
