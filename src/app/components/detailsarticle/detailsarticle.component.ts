import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiarticleService } from 'src/app/services/apiarticle.service';
import { CartService } from 'src/app/services/cart.service';

@Component({
  selector: 'app-detailsarticle',
  templateUrl: './detailsarticle.component.html',
  styleUrls: ['./detailsarticle.component.css']
})
export class DetailsarticleComponent implements OnInit {
   id=this.activteRoute.snapshot.params.id
   article:any
   quantity=1
   prix=1
   cartList=[]
  constructor(private activteRoute:ActivatedRoute , private articleservice:ApiarticleService, private cartservice:CartService) { }

  ngOnInit(): void {
    this.getarticlebyID()

  }
  getarticlebyID() {

    this.articleservice.getarticlebyID(this.id).subscribe(res => {
      this.article=res
      console.log(res)

    })}
    changeqtestock = (quantity: number) => {
      this.quantity = quantity;
    };
  
    addToCart = (article: any) => {
      if (this.quantity) {
        this.cartservice.addToCart({ article, quantity: this.quantity })
        this.loadCart()
      }
    };
  
   loadCart = () => {
    this.cartservice.cartListSubject
      .subscribe(res => {
        this.cartList = res;       
      })
      console.log('add to cart',this.cartList)
  };

}
