import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiauthenticateService } from 'src/app/services/apiauthenticate.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  LoginForm: FormGroup;
  submitted = false;
  roles: any

  constructor(private apiauth: ApiauthenticateService, private route: Router, private formBuilder: FormBuilder) { }



  ngOnInit(): void {
    this.LoginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
  }
  get f() { return this.LoginForm.controls; }

  login() {
    this.submitted = true;
    if (this.LoginForm.invalid) {
      console.log(this.LoginForm.value)
      return;
    }
    this.apiauth.login(this.LoginForm.value).subscribe((res: any) => {
      if (res !== null) {

        localStorage.setItem("state", "0")
        localStorage.setItem("access_token",
          JSON.parse(JSON.stringify(res)).access_token);
        localStorage.setItem("currentuser", JSON.stringify(res.user));
        this.route.navigate(['/'])
        Swal.fire({
          title: 'success',
          icon: 'success',
          text: 'Authentification Successufly',
          footer: '<a href>Why do I have this issue?</a>'
        })
        console.log(res)
        const jwt = res['access_token']
        var decoded: any = jwt_decode(jwt);
        console.log('decoded', decoded)
        localStorage.setItem("role", JSON.stringify(decoded.role))
      }
    }
      , (err) => {
        {
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Authentification Failed',
            footer: '<a href>Why do I have this issue?</a>'
          })
        }
      }
    )
  }
}

function jwt_decode(jwt: any): any {
  throw new Error('Function not implemented.');
}
