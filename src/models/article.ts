import { Deserializable } from "./deserializable.model";

export class Article implements Deserializable{
    id:number;
    nom:String;
    prix:Number;
    image:any;
    code:any;
    tauxtva:any;
    qtestock:any;
    description:any;
    etat:any;
    deserialize(input: any) {
        Object.assign(this, input);
        return this;
    }


   
}
